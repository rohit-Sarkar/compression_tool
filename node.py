#Huffman tree nodes implemented according to the link: https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/Huffman.html
#for the Huffman tree node(implementing the Base class)

class HuffBaseNode:
    def is_leaf(self):
        raise NotImplementedError("Subclasses must implement this method")
    def weight(self):
        raise NotImplementedError("Subclasses must implement this method")

#Huffman tree node(leaf class)
class HuffLeafNode(HuffBaseNode):
    def __init__(self, e1, wt):
        self.element = e1
        self.weight = wt
    def value(self):
        return self.element
    def weight(self):
        return self.weight
    def is_leaf(self):
        return True

#Huffman tree node(Internal class)
class HuffInternalNode(HuffBaseNode):
    def __init__(self, l, r, wt):
        self.left = l
        self.right = r
        self.weight = wt
    def left(self):
        return self.left
    def right(self):
        return self.right
    def weight(self):
        return self.weight
    def is_leaf(self):
        return False
#Implementing the Huffman tree class
class HuffTree:
    def __init__(self, el=None, wt=None, l=None, r=None):
        if el is not None and wt is not None: #is keyword used to test if 2 objects refer to the same object
            self.root = HuffLeafNode(e1, wt)
        elif l is not None and r is not None and w is not None:
            self.root = HuffInternalNode(l, r, wt)
        else:
            raise ValueError("Invalid parameters for constructing Huff Tree")
        
    def root(self):
        return self.root
    def weight(self):
        return self.root.weight() #weight of tree is weight of root
    def __lt__(self, other): 
        if self.weight() < other.weight(): return True #implementing less than for comparison(used for sort)
        else: return False
    def __eq__(self, other):
        return self.weight() == other.weight() #implement equality for comparison
