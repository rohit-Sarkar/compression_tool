from os.path import exists

def char_frequency(file_input, letter):
    file = open(file_input, 'r')
    text = file.read()
    return text.count(letter)

try:
    file_input = str(input("Enter the name of the file: "))
    search_letter = str(input("Enter the letter you want to search: "))

    #f = open(file_input, 'r')
    print(char_frequency(file_input, search_letter))
    #print(f.read())
except FileNotFoundError:   
    print("File not found")